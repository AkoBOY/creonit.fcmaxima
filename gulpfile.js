'use strict';

const gulp = require('gulp'),
      sass = require('gulp-sass'),
      sassGlob = require('gulp-sass-glob'),
      concat = require('gulp-concat'),
      cleanCSS = require('gulp-clean-css'),
      uglify = require('gulp-uglify'),
      debug = require('gulp-debug'),
      browserSync = require('browser-sync'),
      fileinclude = require('gulp-file-include'),
      imagemin = require('gulp-imagemin');


gulp.task('styles', function(){
  return 	gulp.src('assets/scss/*.scss')
    .pipe(debug())
    .pipe(sassGlob())
    .pipe(sass())
    .on('error', function(e){
      console.log(e);
      this.emit('end');
    })
    .pipe(debug())
    .pipe(concat('all.css'))
    .on('error', sass.logError)
    .pipe(debug())
    .pipe(cleanCSS({
      compatibility: '*',
      level: 1
    }))
    .pipe(browserSync.stream({match: '**/*.scss'}))
    .pipe(gulp.dest('./dest/css'));
});

gulp.task('js', function() {
  gulp.src(['assets/js/libs//**/*.js', 'assets/js/app.js'])
    .pipe(debug())
    .pipe(concat('app.js'))
    .pipe(uglify().on('error', function(e){
      console.log(e);
      this.emit('end');
    }))
    .pipe(browserSync.stream({match: '**/*.js'}))
    .pipe(gulp.dest('./dest/js'))
});

gulp.task('fonts', function() {
  gulp.src('assets/fonts//**')
    .pipe(debug())
    .pipe(gulp.dest('./dest/fonts'))
});

gulp.task('images', function() {
  gulp.src('assets/images/*')
    .pipe(imagemin())
    .pipe(gulp.dest('./dest/images'));
});

gulp.task('fileinclude', function() {
  gulp.src(['assets/html/*.html'])
    .pipe(fileinclude({
      prefix: '@@',
      basepath: '@file'
    })).on('error', function(e){
    console.log(e);
    this.emit('end');
  })
    .pipe(gulp.dest('./dest/'));
});

gulp.task('default', ['styles', 'js', 'fonts', 'images', 'fileinclude']);

gulp.task('watch', function(){
  browserSync.init({
    server: {
      baseDir: "./dest/"
    },
    injectChanges: true
  });
  gulp.watch('*.html').on('change', browserSync.reload);
  gulp.watch('assets/scss/**', ['styles']).on('change', browserSync.reload);
  gulp.watch('assets/js/**', ['js']).on('change', browserSync.reload);
  gulp.watch('assets/fonts/**', ['fonts']).on('change', browserSync.reload);
  gulp.watch('assets/images/**', ['images']).on('change', browserSync.reload);
  gulp.watch('assets/html/**', ['fileinclude']).on('change', browserSync.reload);
});
