// Project scripts

// Init slider
var slider = tns({
  container: '.slider',
  controls: true,
  controlsContainer: '.slider__controls',
  items: 1,
  mouseDrag: true,
  nav: false,
  slideBy: 'page'
});

// Slider controls position
$(".slider__controls").css('bottom', $(".slider__outer").outerHeight() - $(".slider__caption").outerHeight() - 45 + 'px');

// Statistics block animation
var el = $(".statistics__man");
var elContainer = $(".statistics");
startAnimation()
function startAnimation() {
  setInterval(function() {
    el.animate({left: elContainer.width() - el.width()},7000, function() {el.css("transform", "scaleX(-1)")});
    el.animate({left: 0},7000, function() {el.css("transform", "scale(1)");});
  }, 10)
}
